Pour utiliser le programme, veuillez déposer un fichier texte en .txt dans le dossier "sources", comme le fichier "exemple.txt".

Dans ce fichier, vous devrez insérer le texte auquel vous souhaitez apporter des modifications pour améliorer sa lecture par l'utilistaeur.

Puis, lancez le programmme Python "adaptation textes.py" et répondez au différentes demandes du programme pour qu'il puisse vous aider au mieux.

C'est-à-dire : donnez le nom du fichier, le nombre de modificataions et le numéro de la (ou des) modidification(s) souhaitée(s).

