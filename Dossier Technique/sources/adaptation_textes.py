# Ce programme prend en entrée un fichier texte .txt et le renvoie en fichier .html avec la modification choisie par le lecteur

# Dictionnaire qui associe chaque lettre à sa majuscule ainsi que les caractères qui correspondent aux accents codés en UTF-8
dico_maj = {
"a":"A",
"b":"B",
"c":"C",
"d":"D",
"e":"E",
"f":"F",
"g":"G",
"h":"H",
"i":"I",
"j":"J",
"k":"K",
"l":"L",
"m":"M",
"n":"N",
"o":"O",
"p":"P",
"q":"Q",
"r":"R",
"s":"S",
"t":"T",
"u":"U",
"v":"V",
"w":"W",
"x":"X",
"y":"Y",
"z":"Z",
"©":"‰",
"¨":"ˆ",
"§":"‡",
"ª":"Š",
"é":"É",
"è":"È",
"ç":"Ç",
"ê":"Ê",
"œ":"Œ",
"â":"Â",
"à":"À",
"\xa0":"€",
"¢":"‚",
"“":"’",
}



def ouverture_fichier(nom) :
    """Fonction qui ouvre le fichier .txt qu'on souhaite modifié et le transfert dans une liste"""
    fichier = open(nom)
    texte = []
    for x in fichier :
        texte.append(x)

    fichier.close()

    return texte



def entree_programme() :
    """Fonction qui demande et renvoi le nom du fichier .txt auquel il faut apporter des modifications"""
    nom_txt = input("Veuillez indiquer le nom du fichier texte à traiter : ")

    # Rajoute l'extension .txt au fichier si elle n'a pas été mise par l'utilistateur
    if not(nom_txt[-1] == "t" and nom_txt[-2] == "x" and nom_txt[-3] == "t" and nom_txt[-4] == ".") :
        nom_txt += ".txt"

    print(" ")
    print("Les différentes adptations possibles : ")
    print(' - 1) Police "Open dyslexic"')
    print(" - 2) Couleur alternée")
    print(" - 3) Gras alterné")
    print(" - 4) Première lettre des mots en majuscule")
    print(" - 5) Première lettre des mots en gras")
    print(" ")

    return nom_txt



def selection_modifications(texte) :
    """Fonction qui demande le nombre et les numéros de la modification, et active les fonctions corresponadantes aux potentielles modifications."""
    nb_modif = int(input("Combien de modifications souhaitez-vous apporter à votre texte : "))

    # Demande et vérifie si le nombre de modifications souhaitées est correct
    while not(1 <= nb_modif <= 5) :
        print("Veuillez choisir un nombre compris entre 1 et 5.")
        nb_modif = int(input("Combien de modifications souhaitez-vous apporter à votre texte : "))

    liste_modifications = []

    # Demande et vérifie si les numéros des modifications souhaitées sont correct, puis les note dans une liste
    for i in range(nb_modif) :
        num_modif = int(input("Indiquez le numéro d'une des modifications proposées : "))

        while not(1 <= nb_modif <= 5) :
            print("Veuillez choisir un nombre compris entre 1 et 5.")
            num_modif = int(input("Indiquez le numéro d'une des modifications proposées : "))

        while num_modif in liste_modifications :
            print("Vous avez déjà choisi cette modification. Veuillez en choisr une autre.")
            num_modif = int(input("Indiquez le numéro d'une des modifications proposées : "))

        liste_modifications.append(num_modif)



    # Lance les fonctions correspondantes à chacune des modifications souhaitées en respectant un ordre précis
    # pour éviter que les modifications se gênent entre-elles
    if 4 in liste_modifications :
        texte = premiere_lettre_en_maj(texte)

    if 5 in liste_modifications :
        texte = premiere_lettre_en_gras(texte)

    if 2 in liste_modifications :
        texte = couleurs_alternee(texte)

    if 3 in liste_modifications :
        texte = gras_alterne(texte, liste_modifications)

    if 1 in liste_modifications :
        texte = open_dyslexic(texte)


    return texte



def premiere_lettre_en_maj(texte) :
    """Fonction qui met en majuscule la première lettre de chaque mot"""
    nv_texte = []
    for ligne in texte :

        mots = ligne.split(" ")
        nv_ligne = []

        for mot in mots :
            nv_mot = []
            for i in range(len(mot)) :

                # Modifie la lettre de chaque mot en la remplaçant par la majuscule correspondante en utilisant le dictionnaire "dico_maj()"
                if i == 1 :
                    # Permet de vérifier si la première lettre du mot est un accent, codé en UTF-8
                    if mot[0] == "Ã" and mot[1] in dico_maj:
                        nv_mot.append(dico_maj[mot[1]])
                    else :
                        nv_mot.append(mot[1])

                else :
                    if i == 0 and (mot[0] in dico_maj):
                        nv_mot.append(dico_maj[mot[0]])
                    else :
                        nv_mot.append(mot[i])

            nv_mot = "".join(nv_mot)
            nv_ligne.append(nv_mot)


        nv_ligne = " ".join(nv_ligne)
        nv_texte.append(nv_ligne)

    return nv_texte



def premiere_lettre_en_gras(texte) :
    """Fonction qui met en gras la première lettre de chaque mot"""
    nv_texte = []
    for ligne in texte :

        mots = ligne.split(" ")
        nv_ligne = []

        for mot in mots :
            nv_mot = []
            for i in range(len(mot)) :

                # Rajoute la balise HTML "<b>" avant chaque première lettre des mots pour la mettre en gras
                if i == 0 :
                    if mot[0] == "Ã" :
                        nv_mot.append("<b>"+ mot[0] + mot[1] + "</b>")
                    else :
                        nv_mot.append("<b>"+ mot[0] + "</b>")
                else :
                    if not(mot[0] == "Ã" and i == 1) :
                        nv_mot.append(mot[i])



            nv_mot = "".join(nv_mot)
            nv_ligne.append(nv_mot)


        nv_ligne = " ".join(nv_ligne)
        nv_texte.append(nv_ligne)

    return nv_texte



def couleurs_alternee(texte):
    """Fonction qui retourne le texte à la ligne à chaque phrase, et alterne la couleur de chaque ligne."""
    nv_texte = []
    couleur = 1
    compteur = 0

    for ligne in texte :

        if compteur == 0 :
            nv_ligne = "<span class=bleu>"
        else :
            nv_ligne = ""

        nb_caracteres_restants = len(ligne)

        for caractere in ligne :

            nb_caracteres_restants -= 1

            # Rajoute à chaque fin de phrase, détectées grâce à . ou ! ou ?, la balise "<span class=...>"
            # en fonction de la couleur précédente, grâce à la variable "couleur"
            if caractere == "." or caractere == "!" or caractere == "?" :
                nv_ligne += caractere
                if couleur == 1 :
                    nv_ligne += "<span class=rouge>"
                    couleur = 2
                else :
                    nv_ligne += "<span class=bleu>"
                    couleur = 1

                if not nb_caracteres_restants < 2 :
                    nv_ligne += "<br>"

            else :
                nv_ligne += caractere


        compteur += 1

        nv_texte.append(nv_ligne)


    return nv_texte



def gras_alterne(texte, liste_modif):
    """Fonction qui retourne le texte à la ligne à chaque phrase, et met en gras une ligne sur deux."""
    nv_texte = []
    couleur = 1
    compteur = 0

    for ligne in texte :

        if compteur == 0 :
            nv_ligne = "<b>"
        else :
            nv_ligne = ""

        nb_caracteres_restants = len(ligne)

        for caractere in ligne :

            nb_caracteres_restants -= 1
            # Rajoute à chaque fin de phrase, détectées grâce à . ou ! ou ?, la balise "<b>" ou "</b>"
            # en fonction de la mise en forme précédente, grâce à la variable "couleur"

            if caractere == "." or caractere == "!" or caractere == "?" :
                nv_ligne += caractere
                if couleur == 1 :
                    nv_ligne += "</b>"
                    couleur = 2
                else :
                    nv_ligne += "<b>"
                    couleur = 1

                if not(nb_caracteres_restants < 2) and not(2 in liste_modif) :
                    nv_ligne += "<br>"

            else :
                nv_ligne += caractere


        compteur += 1

        nv_texte.append(nv_ligne)

    return nv_texte



def open_dyslexic(texte):
    """Fonction qui applique la police Open Dyslexic au texte"""
    nv_texte= []
    compteur = 0

    for ligne in texte :

        # Rajoute la balise "<span class=opendyslexic>" pour activer la police Open Dyslexic
        nv_ligne = ""
        if compteur == 0 :
            nv_ligne += "<span class=opendyslexic>" + ligne
        else :
            nv_ligne += ligne

        nv_texte.append(nv_ligne)

    return nv_texte



def creation_fichier(texte, nom_fichier_source):
    """Fonction qui crée le fichier en ajoutant les éléments nécessaires pour qu'il devienne un fichier en .html"""
    nom = ""

    for i in range(len(nom_fichier_source)-4):
        nom += nom_fichier_source[i]

    nom += " modifié.html"

    # Crée un fichier HTML en ajoutant, au début les inforamtions dans <head>, le texte avec les modifications qui ont été apportées,
    # et enfin en fermant les balises
    nv_fichier = open(nom, "w")
    nv_fichier.write("<!DOCTYPE html><html lang='fr'><head><meta charset='utf-8' name='viewport' content='width=device-width, minimum-scale=0.1, maximum-scale=3'/><title>ReadMate</title><link rel='stylesheet' href='ressources/style/style.css' /></head><body><div id='titre'><img src='ressources/images/Titre_Bienvenue sur Readmate.png' alt='Bienvenue sur Readmate ! Le site facile qui vous permet de lire avec efficacité'></div><div id='barre_bleue'> </div><div id='zone2'><img src='ressources/images/Voici votre texte.png' alt='Voici votre texte :'><div id='zone3'><p>")
    nv_fichier.write("<br>".join(texte))
    nv_fichier.write("</p></div></div></body>")
    nv_fichier.close()




############################### PROGRAMME PRINCIPAL #############################################################################################




nom_fichier_source = entree_programme()

texte = ouverture_fichier(nom_fichier_source)

texte = selection_modifications(texte)

creation_fichier(texte, nom_fichier_source)

print(" ")
print('Modification du texte terminée.')
print('Le fichier HTML, dont le nom correspond au nom du fichier initial auquel a été rajouté " modifié" est apparu dans le dossier "sources".')




